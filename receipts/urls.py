from django.urls import path

from receipts.views import (ReceiptListView)
from receipts.views import (ReceiptCreateView)
from receipts.views import (ExpenseCategoryListView)
from receipts.views import (AccountListView)
from receipts.views import (ExpenseCategoryCreateView)
from receipts.views import (AccountCreateView)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("categories/", ExpenseCategoryListView.as_view(), name="expense_category"),
    path("account/", AccountListView.as_view(), name="account"),
    path("categories/create/", ExpenseCategoryCreateView.as_view(), name="list_categories"),
    path("account/create/", AccountCreateView.as_view(), name="create_account"),
]
