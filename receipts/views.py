from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
# Create your views here.

class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    content_object_name = "receipt"
    template_name = "receipt.html"
    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "create.html"
    fields = ["vendor","total","tax","date", "category","account"]
    def form_valid(self, form):
        item = form.save(commit=False)
        item.user_property = self.request.user
        item.save()
        return redirect("home")

class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    content_object_name = "expensecategory"
    template_name = "category.html"
    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)

class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    content_object_name = "account"
    template_name = "account.html"
    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)

class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "create_category.html"
    fields = ["name"]
    success_url = reverse_lazy("expense_category")
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "c_account.html"
    fields = ["name", "number"]
    success_url = reverse_lazy("account")
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)