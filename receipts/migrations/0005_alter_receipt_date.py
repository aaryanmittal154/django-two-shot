# Generated by Django 4.1.1 on 2022-09-11 16:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0004_account_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(auto_now=True),
        ),
    ]
